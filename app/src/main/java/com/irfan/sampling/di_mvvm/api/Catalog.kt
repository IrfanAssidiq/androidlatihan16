package com.irfan.sampling.di_mvvm.api

import com.irfan.sampling.di_mvvm.data.SearchResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


/**
 *   created by Irfan Assidiq on 4/20/19
 *   email : assidiq.irfan@gmail.com
 **/
interface Catalog {
    @GET("catalog/v4/search")
    fun search(@Query("q")
    q: String, @Query("limit") limit: Int = 10,
               @Query("offset") offset: Int=0)
    :Call<SearchResponse>
}