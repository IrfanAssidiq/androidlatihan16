package com.irfan.sampling.di_mvvm.depinj

import android.app.Application
import android.content.Context
import dagger.Component
import javax.inject.Singleton
import com.irfan.sampling.di_mvvm.Repo.CatalogRepository
import com.irfan.sampling.di_mvvm.depinj.modules.ApiModule
import com.irfan.sampling.di_mvvm.depinj.modules.AppModule
import com.irfan.sampling.di_mvvm.depinj.modules.NetModule
import com.irfan.sampling.di_mvvm.depinj.modules.RepositoryModule
import com.irfan.sampling.di_mvvm.viewmodel.ProductViewModel


@Singleton
@Component(
        modules = arrayOf(AppModule::class,
            NetModule::class,
            RepositoryModule::class,
            ApiModule::class)
)
interface AppComponent {
    fun inject(viewModelModule: ProductViewModel)
    fun inject(activity: Context)

    fun provideCatalogRepository(): CatalogRepository

    companion object Factory{
        fun create(app: Application, baseUrl: String): AppComponent {
            val appComponent = DaggerAppComponent.builder().
                    appModule(AppModule(app)).
                    apiModule(ApiModule()).
                    netModule(NetModule(baseUrl)).
                    repositoryModule(RepositoryModule()).
                    build()
            return appComponent
        }
    }
}