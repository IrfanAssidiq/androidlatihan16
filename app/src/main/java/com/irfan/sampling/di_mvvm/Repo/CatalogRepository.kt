package com.irfan.sampling.di_mvvm.Repo

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.irfan.sampling.di_mvvm.api.Catalog
import com.irfan.sampling.di_mvvm.data.SearchResponse
import com.irfan.sampling.di_mvvm.viewmodel.Resource
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject


/**
 *   created by Irfan Assidiq on 4/20/19
 *   email : assidiq.irfan@gmail.com
 **/
class CatalogRepository @Inject constructor(val api: Catalog ) {

    fun doSearch(q: String, limit: Int = 1, offset: Int = 0): LiveData<Resource<SearchResponse>> {
        val data = MutableLiveData<Resource<SearchResponse>>();

        api.search(q, limit, offset).enqueue(object : Callback<SearchResponse> {
            override fun onResponse(call: Call<SearchResponse>?, response: Response<SearchResponse>?) {
                data.value = Resource.success(response?.body());
            }

            override fun onFailure(call: Call<SearchResponse>?, t: Throwable?) {
                val exception = AppException(t)
                data.value = Resource.error( exception)
            }
        });
        return data;
    }

}