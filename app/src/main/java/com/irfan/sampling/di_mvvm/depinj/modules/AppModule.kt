package com.irfan.sampling.di_mvvm.depinj.modules

import android.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


/**
 *   created by Irfan Assidiq on 4/20/19
 *   email : assidiq.irfan@gmail.com
 **/
@Module
class AppModule(val application: Application) {
    @Provides
    @Singleton
    fun provideApplication() : Application {
        return application;
    }
}
