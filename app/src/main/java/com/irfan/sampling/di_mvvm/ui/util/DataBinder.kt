package com.irfan.sampling.di_mvvm.ui.util

import android.databinding.BindingAdapter
import android.widget.ImageView
import com.bumptech.glide.Glide


class DataBinder {

    companion object {
        @JvmStatic
        @BindingAdapter("app:imageUrl")
        fun setImageUrl(imageView: ImageView, url: String) {
            val context = imageView.getContext()
            Glide.with(context).load(url).into(imageView);
            //Picasso.with(context).load(url).fit().centerInside().error(R.drawable.no_image).placeholder(R.drawable.no_image).into(imageView);
        }
    }
}