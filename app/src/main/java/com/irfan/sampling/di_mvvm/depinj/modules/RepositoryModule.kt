package com.irfan.sampling.di_mvvm.depinj.modules

import com.irfan.sampling.di_mvvm.Repo.CatalogRepository
import com.irfan.sampling.di_mvvm.api.Catalog
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


/**
 *   created by Irfan Assidiq on 4/20/19
 *   email : assidiq.irfan@gmail.com
 **/
@Module
class RepositoryModule {
    @Provides
    @Singleton
    fun provideCatalogRepository(api: Catalog): CatalogRepository {
        return CatalogRepository(api)
    }
}