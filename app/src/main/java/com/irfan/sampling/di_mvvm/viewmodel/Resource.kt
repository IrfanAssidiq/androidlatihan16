package com.irfan.sampling.di_mvvm.viewmodel

import com.irfan.sampling.di_mvvm.viewmodel.Resource.Status.ERROR
import com.irfan.sampling.di_mvvm.viewmodel.Resource.Status.LOADING
import com.irfan.sampling.di_mvvm.viewmodel.Resource.Status.SUCCESS
import com.irfan.sampling.di_mvvm.Repo.AppException

class Resource<T> private constructor(
        val status: Resource.Status,
        val data: T?,
        val exception: AppException?) {

    enum class Status {
        SUCCESS, ERROR, LOADING
    }

    companion object {

        fun <T> success(data: T?): Resource<T> {
            return Resource(SUCCESS, data, null)
        }

        fun <T> error(exception: AppException?): Resource<T> {
            return Resource(ERROR, null, exception)
        }

        fun <T> loading(data: T?): Resource<T> {
            return Resource(Status.LOADING, data, null)
        }
    }
}