package com.irfan.sampling.di_mvvm.data

import com.google.gson.annotations.SerializedName
import java.util.ArrayList


/**
 *   created by Irfan Assidiq on 4/20/19
 *   email : assidiq.irfan@gmail.com
 **/
data class Product(val id:String,
                   val title: String = "",
                   val images: List<Image> = ArrayList(),
                   val longDescription: String = "",
                   val shortDescription: String = "")
data class Image(@SerializedName("key")
                 val size: String = "M", @SerializedName("url")
                 val url: String = "")
data class SearchResponse (val products: List<Product> = ArrayList())
