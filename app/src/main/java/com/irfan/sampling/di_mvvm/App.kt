package com.irfan.sampling.di_mvvm

import android.app.Application
import com.irfan.sampling.di_mvvm.depinj.AppComponent

class App: Application() {
    companion object {
        //platformStatic allow access it from java code
        @JvmStatic lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        appComponent = AppComponent.create(this, "https://api.bol.com/");
    }


}