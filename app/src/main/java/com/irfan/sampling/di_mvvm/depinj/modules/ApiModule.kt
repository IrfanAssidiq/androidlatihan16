package com.irfan.sampling.di_mvvm.depinj.modules

import com.irfan.sampling.di_mvvm.api.Catalog
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton


/**
 *   created by Irfan Assidiq on 4/20/19
 *   email : assidiq.irfan@gmail.com
 **/
@Module
class ApiModule {
    @Provides
    @Singleton
    fun providesCatalogApi(retrofit: Retrofit) :Catalog{
        return retrofit
            .create<Catalog>(
                Catalog::class.java
            )
    }
}